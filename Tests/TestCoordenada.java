import static org.junit.Assert.*;

import org.junit.Test;

import CapaDomini.Coordenada;
public class TestCoordenada {
	
	@Test
	public void testSintaxisString() {
		try {

			Coordenada a = new Coordenada(0,1);
			assertEquals("Sintaxis de string incorrecta", "[0,1]", a.toString());
		}
		catch (Exception e){
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testIgualtatCoordenades() {
		try {
			Coordenada a = new Coordenada(0,1);
			Coordenada b = new Coordenada(1,0);
			Coordenada c = new Coordenada(0,-1);
			Coordenada d = new Coordenada(1,0);
			assertNotEquals("Coordenades desordenades mal definides, no haurien de ser iguals", a, b);
			assertNotEquals("Coordenades negatives mal definides, no haurien de ser iguals", a, c);
			assertNotEquals("Coordenades negatives i desordenades mal definides, no haurien de ser iguals", b, c);
			assertEquals("Coordenades mal definides, haurien de ser iguals", b, d);
	}
		catch (Exception e){
			fail(e.getMessage());
		}
	}
}

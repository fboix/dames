package CapaDomini;

import javax.swing.JOptionPane;

import CapaPersistencia.BBDDPartida;
import CapaPersistencia.BBDDTaulell;

public class Partida {

	public static enum EstatPartida{EnCurs, GuanyenBlanques, GuanyenVermelles, Empat}
	
	//dades que no s'actualitzen a la BBDD
	private int id_partida;//pk
	private Taulell taulell;//fk1 --> INT a la BBDD
	private Jugador jugadorBlanques;//fk2 --> STR a la BBDD
	private Jugador jugadorVermelles;//fk3 --> STR a la BBDD
	//dades que s'actualitzen a la BBDD
	private EstatPartida estat;//només es pot jugar si l'estat és EnCurs, però conta com a estadística sinó --> STR a la BBDD
	private boolean[] movimentsFets;//[0] mogut, [1] menjat --> STR a la BBDD
	private boolean[] movimentsAnteriors;//[0] mogut, [1] menjat --> STR a la BBDD
	private boolean partidaComencada;//per evitar accions indessitjades al inici
	private boolean taulesProposades;//canvia el seguent torn
	private boolean tornBlanques;//controla si un usuari pot o no modificar la partida
	private boolean bufat;//controla que l'usuari només bufi un cop per torn
	
	//constructors, un per partides noves l'altra per partides registrades existents
	public Partida(Jugador userA, Jugador userB) {
		taulell = new Taulell();
		jugadorBlanques = userA;
		jugadorVermelles = userB;
		estat = EstatPartida.EnCurs;
		movimentsFets = new boolean[2];
		movimentsAnteriors = new boolean[2];
		tornBlanques = true;
		taulesProposades = false;
		bufat = false;
		try {id_partida = BBDDPartida.RegistrarNovaPartida(this);}
		catch (Exception e) {e.printStackTrace();}
	}
	public Partida(int IDPartida, Taulell taula, Jugador jugadorBl, Jugador jugadorVe, EstatPartida estatPartida, boolean[] movFets, boolean[] movAnteriors, boolean comencada, boolean taules, boolean torn, boolean buf) {
		id_partida = IDPartida;
		taulell = taula;
		jugadorBlanques = jugadorBl;
		jugadorVermelles = jugadorVe;
		estat = estatPartida;
		movimentsFets = movFets;
		movimentsAnteriors = movAnteriors;
		partidaComencada = comencada;
		taulesProposades = taules;
		tornBlanques = torn;
		bufat = buf;
	}
	
	//Funcions per al controlador, sempre amb el torn comprovat abans
	public void FerMoviment(Coordenada origen, Coordenada desti) {
		if(taulesProposades) {
			String jugada = taulell.FerJugada(origen, desti, !tornBlanques, movimentsFets, true);
			if(jugada.equals("move")||jugada.equals("eat")) FinalitzarPartida(tornBlanques? EstatPartida.GuanyenBlanques:EstatPartida.GuanyenVermelles);
		}
		else {
			String jugada = taulell.FerJugada(origen, desti, tornBlanques, movimentsFets, false);
			JOptionPane.showMessageDialog(null, jugada);
			if(jugada.endsWith("!"))ActualitzarPartida();
			if(movimentsFets[1]) ComprovarPartidaExhaurida();
		}
	}
	public void CanviarTorn() {
		if(!partidaComencada) partidaComencada = true;
		tornBlanques = !tornBlanques;
		movimentsAnteriors = movimentsFets;
		movimentsFets = new boolean[2];
		bufat = false;
		ActualitzarPartida();
		//TODO esdeveniment canvi de torn
	}
	private void ComprovarPartidaExhaurida() {
		if(taulell.GetBlanquesRestants()==0) FinalitzarPartida(EstatPartida.GuanyenVermelles);
		else if(taulell.GetVermellesRestants()==0) FinalitzarPartida(EstatPartida.GuanyenBlanques);
	}
	public void Bufar(Coordenada origen, Coordenada desti) {
		if(taulell.FerJugada(origen, desti, !tornBlanques, new boolean[] {false, false}, true).equals("eat")) {
			MostrarTaulellActual();
			taulell.BufarFitxa(origen, !tornBlanques);
			ComprovarPartidaExhaurida();
			JOptionPane.showMessageDialog(null, "Fitxa bufada, ben vist!");
			bufat = true;
			ActualitzarPartida();
			//TODO esdeveniment bufada
		}
		else JOptionPane.showMessageDialog(null, "Moviment proposat incorrecte, no es bufa");
	}
	public void Taules() {
		if(taulesProposades) FinalitzarPartida(EstatPartida.Empat);
		else {
			taulesProposades = true;
			//TODO canviar text del botó de taules a "Acceptar taules"?
			CanviarTorn();
		}
	}
	public void Rendirse(Jugador perdedor) {
		FinalitzarPartida(perdedor.equals(jugadorBlanques)? EstatPartida.GuanyenVermelles:EstatPartida.GuanyenBlanques);
	}
	private void FinalitzarPartida(EstatPartida estatFinal) {
		estat = estatFinal;
		ActualitzarPartida();
		String missatgeGuanyador;
		switch(estatFinal) {
		case GuanyenBlanques: missatgeGuanyador = jugadorBlanques.GetNom()+" ha guanyat la partida amb les fitxes blanques!";break;
		case GuanyenVermelles: missatgeGuanyador = jugadorVermelles.GetNom()+" ha guanyat la partida amb les fitxes vermelles!";break;
		default: missatgeGuanyador = "Ningú guanya! "+jugadorBlanques.GetNom()+" ha fet perdre el temps a "+jugadorVermelles.GetNom();
		}
		try {BBDDTaulell.EliminarTaulell(taulell);} //Un cop acabada, una partida només serveix per saber-ne el resultat, i potser comprovar l'historial
		catch (Exception e) {}
		JOptionPane.showMessageDialog(null, missatgeGuanyador);
		//TODO esdeveniment partida acabada (personalitzada)
	}
	
	//Actualitza les dades actualitzables al canviar de torn o acabar partida, I CADA COP QUE ES TIRA!
	private void ActualitzarPartida() {
		try {BBDDPartida.ActualitzarPartida(this);}
		catch (Exception e) {e.printStackTrace();}
	}
	
	//Dades per al controlador
	public boolean EsElMeuTorn(Jugador qui) {return (qui.equals(jugadorBlanques)==tornBlanques);}
	public boolean HeMogut() {return (movimentsFets[0]||movimentsFets[1]);}
	public boolean AnteriorHaMenjat() {return movimentsAnteriors[1];}
	public void MostrarTaulellAnterior() {taulell.PintarTaulellAnterior();}
	public void MostrarTaulellActual() {taulell.PintarTaulellActual();}
	public EstatPartida GetEstat() {return estat;}
	public static EstatPartida GetEstat(String estat) {
	      switch(estat) {
	      case "en_curs":return EstatPartida.EnCurs;
	      case "guanyen_blanques":return EstatPartida.GuanyenBlanques;
	      case "guanyen_vermelles":return EstatPartida.GuanyenVermelles;
	      default: return EstatPartida.Empat;
	      }
	}
	public boolean HiHaJugador(Jugador qui) {return (qui.equals(jugadorBlanques)||qui.equals(jugadorVermelles));}
	@Override
    public boolean equals(Object o) {
        if (o == this) return true; 
        if (!(o instanceof Partida)) return false;
        Partida p = (Partida) o; 
        return p.GetIdPartida()==id_partida;
    }
	
	//Dades traduides per a la bbdd
	public int GetIdPartida() {return id_partida;}
	public boolean GetTornBlanques() {return tornBlanques;}
	public boolean GetPartidaComencada() {return partidaComencada;}
	public boolean GetTaulesProposades() {return taulesProposades;}
	public boolean GetBufat() {return bufat;}
	public int GetTaulell() {return taulell.GetIDTaulell();}
	public String GetJugadorBlanques() {return jugadorBlanques.GetNom();}
	public String GetJugadorVermelles() {return jugadorVermelles.GetNom();}
	public String GetMovimentsFets() {return ((movimentsFets[0]? "t":"f")+","+(movimentsFets[1]? "t":"f"));}
	public String GetMovimentsAnteriors() {return ((movimentsAnteriors[0]? "t":"f")+","+(movimentsAnteriors[1]? "t":"f"));}
	public String GetEstatPartida() {
		switch(estat) {
		case EnCurs: return "en_curs";
		case GuanyenBlanques: return "guanyen_blanques";
		case GuanyenVermelles: return "guanyen_vermelles";
		case Empat: return "empat";
		default: return "error";
		}
	}
	
	public Jugador GetBlanques() {return jugadorBlanques;}
	public Jugador GetVermelles() {return jugadorVermelles;}
}

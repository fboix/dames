package CapaDomini;

public class Coordenada {
	
	
	private int x;
	private int y;
	
	public Coordenada(int x, int y) {
		this.x=x;
		this.y=y;
	}
	
	public int GetX() {return x;}
	public int GetY() {return y;}
	public void SetX(int nX) {x=nX;}
	public void SetY(int nY) {y=nY;}
	
	@Override
    public boolean equals(Object o) { 
        if (o == this) return true; 
        if (!(o instanceof Coordenada)) return false;
        Coordenada c = (Coordenada) o; 
        return c.GetX()==x&&c.GetY()==y;
    }
	@Override
	public String toString() {
		return (x+","+y);
	}
} 

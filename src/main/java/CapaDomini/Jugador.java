package CapaDomini;

import java.util.ArrayList;

import CapaPersistencia.BBDDJugador;
import CapaPersistencia.BBDDPartida;
import CapaPersistencia.BBDDSolicituds;

public class Jugador {

	//cap d'aquestes dades s'actualitza a la bbdd, es registren al menú de registre
	private String nom;//pk
	private String pass;
	private ArrayList<Partida> partides;//no pertanyen a la BBDD del jugador
	private ArrayList<Solicitud> solicitudsPartida; //no pertanyen a la BBDD del jugador
	//dades que s'actualitzen a la bbdd, com a solució simple i temporal a la manca d'admistrament de sessions
	private boolean online;
	
	public Jugador(String nomUsuari, String contrassenya, boolean actiu) {
		nom = nomUsuari;
		pass = contrassenya;
		partides = new ArrayList<Partida>();
		solicitudsPartida = new ArrayList<Solicitud>();
		online = actiu;
	}
	
	public String GetNom() {return nom;}
	public String GetContrassenya() {return pass;}
	public boolean IsOnline() {return online;}
	
	public void CanviarSessio(boolean oberta) {
		online = oberta;
		try {
			BBDDJugador.ActualitzarSessio(this);
			for(Solicitud sol:solicitudsPartida) BBDDSolicituds.EliminarSolicitud(sol);
			}
		catch (Exception e) {e.printStackTrace();}
	}
	
	public void AfegirSolicitud(Solicitud peticio) {solicitudsPartida.add(peticio);}
	public void EliminarSolicitud(Solicitud peticio) {solicitudsPartida.remove(peticio);}
	public void AfegirPartida(Partida partida) {partides.add(partida);}
	
	public ArrayList<Partida> RefrescarPartides() throws Exception {
		partides = new ArrayList<Partida>();
		BBDDPartida.OmplirPartidesJugador(this);
		return partides;
	}
	public int[] GetEstadistiquesContra(Jugador jugador) {
		int[] stats = new int[3];//totals, guanyades, perdudes
		for(Partida partida:partides) {
			if(partida.HiHaJugador(jugador)) {
				switch(partida.GetEstat()) {
				case EnCurs:break;
				case GuanyenBlanques:
					if(partida.GetJugadorBlanques().equals(nom))++stats[1];
					else ++stats[2];
					++stats[0];break;
				case GuanyenVermelles:
					if(partida.GetJugadorVermelles().equals(nom))++stats[1];
					else ++stats[2];
					++stats[0];break;
					default:++stats[0];break;
				}
			}
		}
		return stats;
	}
	public ArrayList<Solicitud> RefrescarSolicituds() throws Exception {
		solicitudsPartida = new ArrayList<Solicitud>();
		BBDDSolicituds.OmplirSolicitudsJugador(this);
		return solicitudsPartida;
	}
	
	@Override
    public boolean equals(Object o) {
        if (o == this) return true; 
        if (!(o instanceof Jugador)) return false;
        Jugador j = (Jugador) o; 
        return j.GetNom().equals(nom);
    }
}

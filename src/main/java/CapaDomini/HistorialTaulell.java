package CapaDomini;

import java.util.ArrayList;

import CapaPersistencia.BBDDHistorialTaulell;

public class HistorialTaulell {
	
	//necessari per facilitar l'adaptació del nou historial
	private static final char[][] taulellInicial = {{Taulell.empty,Taulell.red,Taulell.empty,Taulell.red,Taulell.empty,Taulell.red,Taulell.empty,Taulell.red},
													{Taulell.red,Taulell.empty,Taulell.red,Taulell.empty,Taulell.red,Taulell.empty,Taulell.red,Taulell.empty},
													{Taulell.empty,Taulell.red,Taulell.empty,Taulell.red,Taulell.empty,Taulell.red,Taulell.empty,Taulell.red},
													{Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty},
													{Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty,Taulell.empty},
													{Taulell.white,Taulell.empty,Taulell.white,Taulell.empty,Taulell.white,Taulell.empty,Taulell.white,Taulell.empty},
													{Taulell.empty,Taulell.white,Taulell.empty,Taulell.white,Taulell.empty,Taulell.white,Taulell.empty,Taulell.white},
													{Taulell.white,Taulell.empty,Taulell.white,Taulell.empty,Taulell.white,Taulell.empty,Taulell.white,Taulell.empty}};
	//dades que no s'actualitzen
	private int id_historial;//pk
	//dades que s'actualitzen
	private ArrayList<Coordenada[]> moviments;//grups de dues coordenades separades per '>', separats per '|'. Si la segona coordenada es null, es posa 'x' (bufar)
	
	//constructors, un per historials d'un taulell nou, i l'altre per historials registrats a la BBDD
	public HistorialTaulell() {
		moviments = new ArrayList<Coordenada[]>();
		try {id_historial = BBDDHistorialTaulell.RegistrarNouHistorial(this);}
		catch (Exception e) {e.printStackTrace();}
	}
	public HistorialTaulell(int IDHistorial, ArrayList<Coordenada[]> moves) {
		id_historial = IDHistorial;
		moviments = moves;
	}
	
	//métodes de "visualització" d'un estat específic del taulell
	public char[][] GetTaulellAcutal(){
		return GetEstatAnterior(0);
	}
	public char[][] GetUltimTaulell(){
		return GetEstatAnterior(1);
	}
	public char[][] GetEstatAnterior(int anterioritat){
		char[][] taulell = CopiarArray(taulellInicial);
		if(anterioritat<=moviments.size()) {
			for(int i=0;i<moviments.size()-anterioritat;++i) {
				Coordenada[] moviment = moviments.get(i);
				if(moviment[1]!=null) {
					char casella = taulell[moviment[0].GetY()][moviment[0].GetX()];
					taulell[moviment[1].GetY()][moviment[1].GetX()] = casella;
					int migX = moviment[1].GetX();
					int migY = moviment[1].GetY();
					migX += migX>moviment[0].GetX()? -1:1;
					migY += migY>moviment[0].GetY()? -1:1;
					taulell[migY][migX] = Taulell.empty; //Sempre que es mou, la casella anterior al destí (desde l'origen) acaba buida
					if(casella==Taulell.white||casella==Taulell.red) {
						boolean blanca = casella==Taulell.white;
						if(moviment[1].GetY()==(blanca? 0:7)) taulell[moviment[1].GetY()][moviment[1].GetX()] = blanca? Taulell.whiteQueen:Taulell.redQueen; //simula la coronació
					}
				}
				taulell[moviment[0].GetY()][moviment[0].GetX()] = Taulell.empty;
				
			}
		}
		return taulell;
	}
	public Coordenada TrobarUltimMoviment(Coordenada ultimOrigen) {
		for(int i=moviments.size()-1;i>=0;--i) if(moviments.get(i)[0].equals(ultimOrigen)) return moviments.get(i)[1];
		return null;
	}
	
	//métode de modificació de l'historial, que sempre s'actualitzarà d'inmediat a la BBDD
	public void AfegirEstat(Coordenada origen, Coordenada desti) throws Exception {
		moviments.add(new Coordenada[] {origen, desti});
		BBDDHistorialTaulell.ActualitzarHistorial(this);
	}
	private char[][] CopiarArray(char[][] og) {
		char[][] nou = new char[8][8];
		for(int y=0;y<8;++y)for(int x=0;x<8;++x)nou[y][x]=og[y][x];
		return nou;
	}
	
	//Dades traduides per a la BBDD
	public int GetIDHistorial() {return id_historial;}
	@Override
	public String toString() {
		String result = "";
		for(int i=0;i<moviments.size();++i) {
			Coordenada[] moviment = moviments.get(i);
			result+=moviment[0].toString()+">";
			result+=((moviment[1]!=null)? moviment[1].toString():"x");
			if(i<moviments.size()-1)result += ":";
		}
		return result;
	}

}

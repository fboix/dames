package CapaDomini;

import CapaPersistencia.BBDDTaulell;
import CapaPresentacio.PantallaJoc;

public class Taulell {

	public static final char empty = 'e';
	public static final char red = 'r';
	public static final char white = 'w';
	public static final char redQueen = 'R';
	public static final char whiteQueen = 'W';
	
	//dades que no s'actualitzen a la BBDD
	private int id_taulell;//pk
	private HistorialTaulell historial;//fk --> INT a la BBDD
	//dades que s'actualitzen a la BBDD
	private char[][] caselles;//STR a la BBDD
	private Coordenada lastMove;//STR a la BBDD
	private int blanquesRestants;//INT a la BBDD
	private int vermellesRestants;//INT a la BBDD
	
	//constructors, un per taulells nous (de partides noves), l'altre per taulells registrats a la BBDD
	public Taulell() {
		TaulellNou();
		historial = new HistorialTaulell();
		lastMove = new Coordenada(0,0);
		try {id_taulell = BBDDTaulell.RegistrarNouTaulell(this);}
		catch (Exception e) {e.printStackTrace();}
	}
	public Taulell(int id_taulell, char[][] caselles, Coordenada lastMove, HistorialTaulell historial, int blanquesRestants, int vermellesRestants) {
		this.id_taulell = id_taulell;
		this.caselles = caselles;
		this.lastMove = lastMove;
		this.historial = historial;
		this.blanquesRestants = blanquesRestants;
		this.vermellesRestants = vermellesRestants;
	}
	//fa un taulell buit
	private void TaulellNou() {
		caselles = new char[8][8];
		for(int y=0;y<8;++y)for(int x=0;x<8;++x) {
			char casella = empty;
			if(y%2!=x%2) {
				if(y<=2) casella=red;
				else if(y>=5) casella=white;
			}
			caselles[y][x]=casella;
		}
		blanquesRestants = 12;
		vermellesRestants = 12;
	}
	
	//Comprova la llógica d'un moviment de la casella [origen] a la casella [desti], i fa els canvis respectivament.
	//Sembla ser molt extens degut al tractament d'errors detallat, i la redundància óptima de tractament entre el moviment d'una fitxa
	//normal i una reina, però tot això és per la millor lectura i execució clara.
	public String FerJugada(Coordenada origen, Coordenada desti, boolean blanc, boolean[] jugadesFetes, boolean proposant) {
		if(jugadesFetes[1]&&!origen.equals(lastMove)) return "Només pots continuar amb la mateixa fitxa amb la que has menjat";
		if(!jugadesFetes[0]) {
			if(caselles[origen.GetY()][origen.GetX()]!=empty) {
				if(caselles[origen.GetY()][origen.GetX()]==(blanc? white:red)) {
					//fitxa normal
					if(blanc? desti.GetY()<origen.GetY():desti.GetY()>origen.GetY()) {
						//direcció correcta
						if(Math.abs(origen.GetX()-desti.GetX())==2&&Math.abs(origen.GetY()-desti.GetY())==2) {
							//distancia correcta per menjar
							Coordenada victim = new Coordenada((origen.GetX()+desti.GetX())/2,(origen.GetY()+desti.GetY())/2);
							if(EsPotMenjarFitxa(victim, !blanc)) {
								if(EsPotMoureFitxa(origen, desti)) {
									if(proposant) return "eat";
									MenjarFitxa(victim, !blanc);
									MoureFitxa(origen, desti,false, blanc);
									jugadesFetes[1]=true;
									return "Fitxa "+(blanc? "blanca":"vermella")+" ha menjat un enemic "+(blanc? "vermell":"blanc")+" amb éxit!";
								}
								else return "No es pot menjar així";
							}
							else return "No tens cap víctima a la que saltar";
						}
						else if(!jugadesFetes[1]) {
							if (Math.abs(origen.GetX()-desti.GetX())==1&&Math.abs(origen.GetY()-desti.GetY())==1) {
								if(EsPotMoureFitxa(origen,desti)) {
									if(proposant) return "move";
									MoureFitxa(origen, desti,false, blanc);
									jugadesFetes[0]=true;
									return "Fitxa "+(blanc? "blanca":"vermella")+" moguda amb éxit!";
								}
								else return "No et pots moure a una casella plena";
							}
							else return "No et pots moure així";
						}
						else return "No pots moure després de menjar";
					}
					else return "Direcció equivocada";
				}
				else if(caselles[origen.GetY()][origen.GetX()]==(blanc? whiteQueen:redQueen)) {
					//fitxa reina
					if(desti.GetX()!=origen.GetX()&&Math.abs(desti.GetX()-origen.GetX())==Math.abs(desti.GetY()-origen.GetY())) {
						if(EsPotMoureFitxa(origen, desti)) {
							//moviment correcte
							int incX = desti.GetX()>origen.GetX()? 1:-1;
							int incY = desti.GetY()>origen.GetY()? 1:-1;
							for(int i=0;Math.abs(origen.GetX()+i*incX-desti.GetX())>=1;i++) {
								Coordenada victim = new Coordenada(origen.GetX()+i*incX,origen.GetY()+i*incY);
								if(EsPotMenjarFitxa(victim,!blanc)){
									if(Math.abs(origen.GetX()+i*incX-desti.GetX())==1) {
										if(proposant) return "eat";
										MenjarFitxa(victim, !blanc);
										MoureFitxa(origen, desti, true, blanc);
										jugadesFetes[1] = true;
										return "Reina "+(blanc? "blanca":"vermella")+" ha menjat un enemic "+(blanc? "vermell":"blanc")+" amb éxit!";
									}
									else return "Així no es menja amb una reina";
								}
							}
							if(!jugadesFetes[1]) {
								if(proposant) return "move";
								MoureFitxa(origen, desti, true, blanc);
								jugadesFetes[0] = true;
								return "Reina "+(blanc? "blanca":"vermella")+" moguda amb éxit!";
							}
							else return "Un cop menjat amb la reia, no pots moure";
						}
						else return "No et pots moure a una casella plena";
					}
					else return "Una reina es pot moure la distancia que vulgui, però en diagonal";
				}
				else return "Aquesta fitxa no és teva";
			}
			else return "Casella buida";
		}
		else return "Ja t'has mogut aquest torn";
	}
	
	//métodes que comproven la validesa genérica d'una jugada
	private boolean EsPotMenjarFitxa(Coordenada coord, boolean whiteTarget) {
		return (caselles[coord.GetY()][coord.GetX()]==(whiteTarget? white:red)||caselles[coord.GetY()][coord.GetX()]==(whiteTarget? whiteQueen:redQueen));
	}
	private boolean EsPotMoureFitxa(Coordenada origen, Coordenada desti) {
		return caselles[desti.GetY()][desti.GetX()]==empty;
	}
	//Métodes que executen una jugada. Com sempre que es menja també es mou, la funció de moure crida a la que actualitza els canvis a la BBDD
	public void MenjarFitxa(Coordenada coord, boolean whiteVictim) {
		if(whiteVictim)blanquesRestants--;
		else vermellesRestants--;
		caselles[coord.GetY()][coord.GetX()]=empty;
		ActualitzarTaulell(coord,null);
	}
	private void MoureFitxa(Coordenada origen, Coordenada desti, boolean reina, boolean blanca) {
		caselles[desti.GetY()][desti.GetX()]=caselles[origen.GetY()][origen.GetX()];
		caselles[origen.GetY()][origen.GetX()]=empty;
		if(!reina&&desti.GetY()==(blanca? 0:7)) caselles[desti.GetY()][desti.GetX()] = blanca? whiteQueen:redQueen;
		lastMove = desti;
		ActualitzarTaulell(origen, desti);
	}
	public void BufarFitxa(Coordenada coord, boolean whiteVictim) {
		if(caselles[coord.GetY()][coord.GetX()]==empty) MenjarFitxa(historial.TrobarUltimMoviment(coord),whiteVictim);
		else MenjarFitxa(coord, whiteVictim);
	}
	
	//registra els canvis a la BBDD sempre que sigui cridat, que sol ser quan es fa un moviment
	private void ActualitzarTaulell(Coordenada origenJugada, Coordenada destiJugada) {
		try {
			historial.AfegirEstat(origenJugada, destiJugada);
			PantallaJoc.PintarTaulell(caselles);
			BBDDTaulell.ActualitzarTaulell(this);
		}
		catch (Exception e) {e.printStackTrace();}
	}
	//funcions per modificar la pantalla de joc en l'estat actual o l'anterior (ara és "permanentment", tractar amb compte!)
	public void PintarTaulellAnterior() {
		caselles = historial.GetUltimTaulell();
		PantallaJoc.PintarTaulell(caselles);
		}
	public void PintarTaulellActual() {
		caselles = historial.GetTaulellAcutal();
		PantallaJoc.PintarTaulell(caselles);
		}

	//Dades traduides per a la BBDD
	public int GetIDTaulell() {return id_taulell;}
	public String GetLastMove() {return lastMove.toString();}
	public int GetHistorial() {return historial.GetIDHistorial();}
	public int GetBlanquesRestants() {return blanquesRestants;}
	public int GetVermellesRestants() {return vermellesRestants;}
	@Override
	public String toString() {
		String result = "";
		for(int y=0;y<8;++y) {
			for(int x=0;x<8;++x) {
				result+=caselles[y][x];
				if(x<7)result+=",";
				else if(y<7) result+=";";
			}
		}
		return result;
	}

	@Override
    public boolean equals(Object o) {
        if (o == this) return true; 
        if (!(o instanceof Taulell)) return false;
        Taulell t = (Taulell) o; 
        return t.GetIDTaulell()==id_taulell;
    }
}

package CapaDomini;

public class Solicitud {
	
	//les dues son pk, no s'actualitzen a la BBDD, només es creen o destrueixen
	private Jugador emisor, receptor;
	
	public Solicitud(Jugador emisor, Jugador receptor) {
		this.emisor = emisor;
		this.receptor = receptor;
	}
	
	public String GetEmisor() {return emisor.GetNom();}
	public String GetReceptor() {return receptor.GetNom();}
	public boolean SocEmisor(Jugador jugador) {return jugador.equals(emisor);}
	
	public Jugador GetJugadorEmisor() {return emisor;}
	public Jugador GetJugadorReceptor() {return receptor;}
	
	
}

package CapaAplicacio;

import javax.swing.JOptionPane;

import CapaDomini.Coordenada;
import CapaDomini.Jugador;
import CapaDomini.Partida;
import CapaDomini.Partida.EstatPartida;
import CapaPersistencia.BBDDPartida;
import CapaPresentacio.PantallaJoc;

public class ControladorPartida {
	
	//dades específiques de la sessió de cada partida
	private Partida partida;
	private Jugador jugador;
	//dades per facilitar el tractament i mostra del transcurs de la partida
	private boolean proposantBufar;
	private Coordenada selectActual;
	private PantallaJoc finestra;
	
	//constructor, que alhora mostra la pantalla pertinent, per només haver de crear una instància del controlador per jugar
	public ControladorPartida(Partida joc,Jugador usuari) {
		partida = joc;
		jugador = usuari;
		finestra = new PantallaJoc(this);
	}
	
	//métodes del control de la partida, que connecta la partida i la interfície
	public boolean SeleccionarCasella(Coordenada coord) {
		if(partida.EsElMeuTorn(jugador)) {
			if(selectActual==null) {
				selectActual = coord;
				return true;
			}
			else {
				if(proposantBufar) {
					proposantBufar = false;
					partida.Bufar(selectActual, coord);
					RefrescarPartida();
				}
				else partida.FerMoviment(selectActual, coord);
				selectActual = null;
				return false;
			}
		}
		else JOptionPane.showMessageDialog(null, "No és el teu torn");
		return false;
	}
	
	public void ProposarBufar() {
		if(!proposantBufar) {
			if(partida.EsElMeuTorn(jugador)) {
				if(!partida.GetTaulesProposades()) {
					if(!partida.AnteriorHaMenjat()) {
						if(!partida.HeMogut()) {
							if(partida.GetPartidaComencada()) {
								if(!partida.GetBufat()) {
									proposantBufar = true;
									partida.MostrarTaulellAnterior();
									JOptionPane.showMessageDialog(null, "Menja la fitxa que l'oponent podria haver menjat");
								}
								else JOptionPane.showMessageDialog(null, "Ja has bufat aquest torn");
							}
							else JOptionPane.showMessageDialog(null, "No pots bufar al primer torn, inepte!");
						}
						else JOptionPane.showMessageDialog(null, "Només pots bufar al principi del teu torn");
					}
					else JOptionPane.showMessageDialog(null, "L'oponent ja ha menjat en el seu torn");
				}
				else JOptionPane.showMessageDialog(null, "Bufar no conta com a moviment vàlid per l'oponent, has de resoldre les taules legalment");
			}
			else JOptionPane.showMessageDialog(null, "Només pots bufar al teu torn");
		}
		else {
			proposantBufar = false;
			partida.MostrarTaulellActual();
			JOptionPane.showMessageDialog(null, "Bufament cancel·lat");
		}
	}
	public void ProposarTaules() {
		if(!proposantBufar) {
			if(partida.EsElMeuTorn(jugador)) {
				String missatge = (partida.GetTaulesProposades()? "Segur que vols acceptar les taules de l'oponent?\nAl fer-ho, la partida acabarà en empat."
																:"Segur que vols proposar taules?\nAl fer-ho, el teu oponent podrà proposar un moviment vàlid per guanyar.");
				if(JOptionPane.showConfirmDialog(null, missatge, "Taules", JOptionPane.YES_NO_OPTION)==0) {
					if(!partida.GetTaulesProposades()) {
						JOptionPane.showMessageDialog(null, "Taules proposades correctament. El teu torn ha estat passat automàticamet.");
						finestra.CanviarTitol("Esperant resposta de l'oponent...");
					}
					partida.Taules();
				}
			}
			else JOptionPane.showMessageDialog(null, "No és el teu torn");
			}
		else {
			proposantBufar = false;
			partida.MostrarTaulellActual();
			JOptionPane.showMessageDialog(null, "Bufament cancel·lat");
		}
	}
	public void Rendirse() {
		if(!proposantBufar) {
			if(JOptionPane.showConfirmDialog(null, "Rendir-se","Segur que vols rendir-te?",JOptionPane.YES_NO_OPTION)==0) partida.Rendirse(jugador);
		}
		else {
			proposantBufar = false;
			partida.MostrarTaulellActual();
			JOptionPane.showMessageDialog(null, "Bufament cancel·lat");
		}
	}
	public void CanviarTorn() {
		if(!proposantBufar) {
			if(partida.EsElMeuTorn(jugador)) {
				if(!partida.GetTaulesProposades()) {
					if(partida.HeMogut()) {
						partida.CanviarTorn();
						RefrescarPartida();
					}
					else JOptionPane.showMessageDialog(null, "Has de fer com a mínim un moviment");
				}
				else JOptionPane.showMessageDialog(null, "La partida està en taules, mou amb les fitxes de l'oponent o accepta-les!");
			}
			else JOptionPane.showMessageDialog(null, "No és el teu torn");
		}
		else {
			proposantBufar = false;
			partida.MostrarTaulellActual();
			JOptionPane.showMessageDialog(null, "Bufament cancel·lat");
		}
	}
	public void RefrescarPartida() {
		if(!proposantBufar) {
			try {partida = BBDDPartida.GetPartida(partida.GetIdPartida());}
			catch (Exception e) {e.printStackTrace();}
			if(partida.GetTaulesProposades()) {
				JOptionPane.showMessageDialog(null, (partida.EsElMeuTorn(jugador)? "L'oponent ha demanat taules!\nAra pots fer un moviment per l'enemic i guanyar,\n o prem el botó 'Proposar Taules' per acceptar-les."
																					:"Espera a que l'oponent respongui a la proposta de taules."));
				finestra.CanviarTitol((partida.EsElMeuTorn(jugador)? "Fes un moviment vàlid per l'enemic per guanyar!":"Esperant resposta de l'oponent..."));
			}
			else finestra.CanviarTitol((partida.EsElMeuTorn(jugador)? "ET TOCA (":"NO ET TOCA (")+(partida.GetTornBlanques()? "TORN DE LES BLANQUES":"TORN DE LES VERMELLES")+")");
			partida.MostrarTaulellActual();
		}
		else {
			proposantBufar = false;
			partida.MostrarTaulellActual();
			JOptionPane.showMessageDialog(null, "Bufament cancel·lat");
		}
	}
	
	//informació per a la pantalla de joc, alhora de ser tancada
	public EstatPartida GetEstat() {return partida.GetEstat();}
	public boolean SocBlanc() {return partida.GetJugadorBlanques().equals(jugador.GetNom());}
	public Jugador GetJugador() {return jugador;}
	
}

package CapaPresentacio;

import javax.swing.*;

import CapaDomini.Jugador;
import CapaPersistencia.BBDDJugador;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class MenuRegistre extends JFrame implements ActionListener {
	
	private static final String textIdentificar = "Entrar";
	private static final String textRegistrar = "Registrar";
	
	private static MenuRegistre frame;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
        public void run() {
            try {
            	frame = new MenuRegistre();
            	frame.setSize(420, 420);
            	frame.setTitle("Menú de Registre de Dames!©");
            	frame.setResizable(false);
            	frame.setLocationRelativeTo(null);
            	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
                } catch (Exception e) {e.printStackTrace();}
            }
        });
	}
	private JTextField textFieldName;
	private JTextField textFieldNameR;
	private JPasswordField textFieldPassword;
	private JPasswordField textFieldPasswordR1;
	private JPasswordField textFieldPasswordR2;
	
	public MenuRegistre() {
		setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 12));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(Color.LIGHT_GRAY);
		setTitle("Dames!");
		setResizable(false);
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblRegistrarse = new JLabel("Registrar-se");
		lblRegistrarse.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistrarse.setFont(new Font("Poor Richard", Font.PLAIN, 32));
		lblRegistrarse.setBounds(110, 210, 200, 36);
		panel.add(lblRegistrarse);
		
		JLabel lblIdentificarse = new JLabel("Identificar-se");
		lblIdentificarse.setHorizontalAlignment(SwingConstants.CENTER);
		lblIdentificarse.setFont(new Font("Poor Richard", Font.PLAIN, 32));
		lblIdentificarse.setBounds(110, 10, 200, 36);
		panel.add(lblIdentificarse);
		
		textFieldName = new JTextField();
		textFieldName.setToolTipText("nom d'usuari de 3-20 caràcters");
		textFieldName.setBounds(50, 85, 100, 25);
		panel.add(textFieldName);
		textFieldName.setColumns(10);
		
		JLabel lblNom = new JLabel("Nom:");
		lblNom.setFont(new Font("Poor Richard", Font.PLAIN, 16));
		lblNom.setBounds(10, 85, 40, 20);
		panel.add(lblNom);
		
		JLabel lblClau = new JLabel("Clau:");
		lblClau.setFont(new Font("Poor Richard", Font.PLAIN, 16));
		lblClau.setBounds(230, 85, 40, 20);
		panel.add(lblClau);
		
		JButton btnIdentificar = new JButton(textIdentificar);
		btnIdentificar.setBackground(new Color(34, 139, 34));
		btnIdentificar.setFont(new Font("Poor Richard", Font.PLAIN, 16));
		btnIdentificar.setBounds(160, 140, 100, 25);
		btnIdentificar.addActionListener(this);
		panel.add(btnIdentificar);
		
		JButton btnRegistrar = new JButton(textRegistrar);
		btnRegistrar.setBackground(new Color(210, 105, 30));
		btnRegistrar.setFont(new Font("Poor Richard", Font.PLAIN, 16));
		btnRegistrar.setBounds(160, 340, 100, 25);
		btnRegistrar.addActionListener(this);
		panel.add(btnRegistrar);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(0, 190, 420, 20);
		panel.add(separator);
		
		JLabel label = new JLabel("Clau:");
		label.setFont(new Font("Poor Richard", Font.PLAIN, 16));
		label.setBounds(230, 255, 40, 20);
		panel.add(label);
		
		textFieldNameR = new JTextField();
		textFieldNameR.setToolTipText("nom d'usuari de 3-20 caràcters");
		textFieldNameR.setColumns(10);
		textFieldNameR.setBounds(50, 275, 100, 25);
		panel.add(textFieldNameR);
		
		JLabel label_1 = new JLabel("Nom:");
		label_1.setFont(new Font("Poor Richard", Font.PLAIN, 16));
		label_1.setBounds(10, 275, 40, 20);
		panel.add(label_1);
		
		JLabel lblRepetirclau = new JLabel("Repetir Clau:");
		lblRepetirclau.setFont(new Font("Poor Richard", Font.PLAIN, 16));
		lblRepetirclau.setBounds(185, 295, 90, 20);
		panel.add(lblRepetirclau);
		
		textFieldPassword = new JPasswordField();
		textFieldPassword.setToolTipText("contrassenya de 4-20 caràcters");
		textFieldPassword.setEchoChar('*');
		textFieldPassword.setFont(new Font("Poor Richard", Font.PLAIN, 24));
		textFieldPassword.setBounds(275, 85, 125, 25);
		panel.add(textFieldPassword);
		
		textFieldPasswordR1 = new JPasswordField();
		textFieldPasswordR1.setToolTipText("contrassenya de 4-20 caràcters");
		textFieldPasswordR1.setEchoChar('*');
		textFieldPasswordR1.setFont(new Font("Poor Richard", Font.PLAIN, 24));
		textFieldPasswordR1.setBounds(275, 255, 125, 25);
		panel.add(textFieldPasswordR1);
		
		textFieldPasswordR2 = new JPasswordField();
		textFieldPasswordR2.setToolTipText("contrassenya de 4-20 caràcters");
		textFieldPasswordR2.setEchoChar('*');
		textFieldPasswordR2.setFont(new Font("Poor Richard", Font.PLAIN, 24));
		textFieldPasswordR2.setBounds(275, 295, 125, 25);
		panel.add(textFieldPasswordR2);
	}
	@Override
	public void actionPerformed(ActionEvent ae) {
		JButton button = (JButton)ae.getSource();
		if(button.getText().equals(textRegistrar)) {
			String nom = textFieldNameR.getText();
			String pass1 = String.valueOf(textFieldPasswordR1.getPassword());
			String pass2 = String.valueOf(textFieldPasswordR2.getPassword());
			if(pass1.equals(pass2)) {
				if(pass1.length()<=20&&pass1.length()>=4) {
					if(nom.length()<=20&&nom.length()>=3) RegistrarConta(nom, pass1);
					else {
						textFieldNameR.setText("");
						JOptionPane.showMessageDialog(null, "El nom ha de tenir entre 3 i 20 caracters");
					}
				}
				else {
					textFieldPasswordR1.setText("");
					textFieldPasswordR2.setText("");
					JOptionPane.showMessageDialog(null, "La contrassenya ha de tenir entre 4 i 20 caràcters");
				}
			}
			else {
				textFieldPasswordR1.setText("");
				textFieldPasswordR2.setText("");
				JOptionPane.showMessageDialog(null, "Les contrassenyes no coincideixen");
			}
		}
		else IdentificarConta(textFieldName.getText(), String.valueOf(textFieldPassword.getPassword()));
		
	}
	
	private void IdentificarConta(String name, String password) {
		try {
			Jugador usuari = BBDDJugador.GetJugador(name,true);
			if(usuari!=null) {
				if(usuari.GetContrassenya().equals(password)) {
					if(!usuari.IsOnline())Entrar(usuari);
					else JOptionPane.showMessageDialog(null, "Aquesta conta ja té una sessió iniciada");
				}
				else {
					textFieldPassword.setText("");
					JOptionPane.showMessageDialog(null, "Contrassenya incorrecta");
				}
			}
			else {
				textFieldPassword.setText("");
				textFieldName.setText("");
				JOptionPane.showMessageDialog(null, "Nom d'usuari inexistent");
			}
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private void RegistrarConta(String name, String password) {
		try {
			if(!BBDDJugador.ExisteixJugador(name)) {
				Jugador nouUsuari = new Jugador(name, password, false);
				BBDDJugador.InsertJugador(nouUsuari);
				Entrar(nouUsuari);
				JOptionPane.showMessageDialog(null, "Conta registrada amb éxit!\n ¡Benvingut a Dames!©!");
			}
			else {
				textFieldNameR.setText("");
				JOptionPane.showMessageDialog(null, "Aquest nom d'usuari ja està en us");
			}
		} catch (Exception e) {e.printStackTrace();}
	}
	
	private void Entrar(Jugador jugador) {
		jugador.CanviarSessio(true);
		new MenuInicial(jugador);
		dispose();
	}
}

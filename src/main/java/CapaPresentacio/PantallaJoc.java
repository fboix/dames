package CapaPresentacio;

import javax.swing.*;

import CapaAplicacio.ControladorPartida;
import CapaDomini.Coordenada;
import CapaDomini.Partida.EstatPartida;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.awt.Font;
import java.awt.GridLayout;

@SuppressWarnings("serial")
public class PantallaJoc extends JFrame implements ActionListener{
	
	private static HashMap<JButton, Coordenada> botons;
	private static final ImageIcon ImgBlanca = new ImageIcon(new ImageIcon("Blanca.png").getImage().getScaledInstance(80, 80, Image.SCALE_SMOOTH));
	private static final ImageIcon ImgVermella = new ImageIcon(new ImageIcon("Vermella.png").getImage().getScaledInstance(80, 80, Image.SCALE_SMOOTH));
	private static final ImageIcon ImgBlancaReina = new ImageIcon(new ImageIcon("BlancaReina.png").getImage().getScaledInstance(80, 80, Image.SCALE_SMOOTH));
	private static final ImageIcon ImgVermellaReina = new ImageIcon(new ImageIcon("VermellaReina.png").getImage().getScaledInstance(80, 80, Image.SCALE_SMOOTH));
	private JButton activeButton;

	private static final String textPassarTorn = "PASSAR TORN";
	private static final String textDemanarTaules = "Taules";
	private static final String textRendirse = "Rendir-se";
	private static final String textBufar = "Bufar Fitxa";
	private static final String textRefrescar = "REFRESCAR";

	private PantallaJoc frame;
	private ControladorPartida controladorPartida;
	
	public PantallaJoc() {
		setResizable(false);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.WEST);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {0};
		gbl_panel.rowHeights = new int[] {0};
		gbl_panel.columnWeights = new double[]{0.0};
		gbl_panel.rowWeights = new double[]{0.0};
		panel.setLayout(gbl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new GridLayout(20, 1, 15, 0));
		
		JButton btnRefrescar = new JButton(textRefrescar);
		btnRefrescar.setForeground(new Color(0, 0, 0));
		btnRefrescar.setFont(new Font("Poor Richard", Font.PLAIN, 24));
		btnRefrescar.setBackground(new Color(119, 136, 153));
		btnRefrescar.addActionListener(this);
		panel_1.add(btnRefrescar);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setForeground(Color.WHITE);
		panel_1.add(separator_3);
		
		JSeparator separator_14 = new JSeparator();
		separator_14.setForeground(Color.WHITE);
		panel_1.add(separator_14);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.WHITE);
		panel_1.add(separator_1);
		
		JButton btnRendirse = new JButton(textRendirse);
		btnRendirse.setForeground(new Color(255, 255, 255));
		btnRendirse.setFont(new Font("Poor Richard", Font.PLAIN, 18));
		btnRendirse.setBackground(new Color(128, 0, 0));
		btnRendirse.addActionListener(this);
		panel_1.add(btnRendirse);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setForeground(Color.WHITE);
		panel_1.add(separator_4);
		
		JButton btnDemanarTaules = new JButton(textDemanarTaules);
		btnDemanarTaules.setForeground(Color.WHITE);
		btnDemanarTaules.setFont(new Font("Poor Richard", Font.PLAIN, 18));
		btnDemanarTaules.setBackground(new Color(95, 158, 160));
		btnDemanarTaules.addActionListener(this);
		panel_1.add(btnDemanarTaules);
		
		JSeparator separator = new JSeparator();
		separator.setForeground(new Color(255, 255, 255));
		panel_1.add(separator);
		
		JButton btnBufar = new JButton(textBufar);
		btnBufar.setForeground(Color.WHITE);
		btnBufar.setFont(new Font("Poor Richard", Font.PLAIN, 18));
		btnBufar.setBackground(new Color(218, 165, 32));
		btnBufar.addActionListener(this);
		panel_1.add(btnBufar);
		
		JButton btnPassarTorn = new JButton(textPassarTorn);
		btnPassarTorn.setFont(new Font("Poor Richard", Font.PLAIN, 24));
		btnPassarTorn.setBackground(new Color(0, 128, 0));
		btnPassarTorn.addActionListener(this);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setForeground(Color.WHITE);
		panel_1.add(separator_2);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setForeground(Color.WHITE);
		panel_1.add(separator_5);
		
		JSeparator separator_6 = new JSeparator();
		separator_6.setForeground(Color.WHITE);
		panel_1.add(separator_6);
		
		JSeparator separator_7 = new JSeparator();
		separator_7.setForeground(Color.WHITE);
		panel_1.add(separator_7);
		
		JSeparator separator_8 = new JSeparator();
		separator_8.setForeground(Color.WHITE);
		panel_1.add(separator_8);
		
		JSeparator separator_9 = new JSeparator();
		separator_9.setForeground(Color.WHITE);
		panel_1.add(separator_9);
		
		JSeparator separator_10 = new JSeparator();
		separator_10.setForeground(Color.WHITE);
		panel_1.add(separator_10);
		
		JSeparator separator_11 = new JSeparator();
		separator_11.setForeground(Color.WHITE);
		panel_1.add(separator_11);
		
		JSeparator separator_12 = new JSeparator();
		separator_12.setForeground(Color.WHITE);
		panel_1.add(separator_12);
		
		JSeparator separator_13 = new JSeparator();
		separator_13.setForeground(Color.WHITE);
		panel_1.add(separator_13);
		panel_1.add(btnPassarTorn);
		botons = new HashMap<JButton, Coordenada>();
		for(int y=0;y<8;++y)for(int x=0;x<8;++x) {
			JButton button = new JButton();
			if(y%2!=x%2) {
				if(y<=2) button.setIcon(ImgVermella);
				else if(y>=5) button.setIcon(ImgBlanca);
			}
			button.setBackground(y%2!=x%2? Color.darkGray:Color.WHITE);
			button.setBorder(BorderFactory.createLineBorder(Color.yellow));
			button.setBorderPainted(false);
			GridBagConstraints gbc_button = new GridBagConstraints();
			gbc_button.insets = new Insets(0, 0, 0, 0);
			gbc_button.gridx = x;
			gbc_button.gridy = y;
			button.addActionListener(this);
			button.setPreferredSize(new Dimension(100,100));
			panel.add(button, gbc_button);
			botons.put(button, new Coordenada(x,y));
		}
		panel.setVisible(true);
	}
	
	public PantallaJoc(ControladorPartida ctrlPartida) {
		controladorPartida = ctrlPartida;
		EventQueue.invokeLater(new Runnable() {
        public void run() {
            try {
            	frame = new PantallaJoc();
            	frame.setSize(1000, 860);
            	frame.setTitle("Carregant...");
            	frame.setResizable(false);
            	frame.setLocationRelativeTo(null);
            	frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frame.setVisible(true);
                frame.addWindowListener(new WindowAdapter()
                {
                    public void windowClosing(WindowEvent e)
                    {
                    	TornarAlMenu();
                    }
                });
                frame.IniciarComponents(controladorPartida);
                } catch (Exception e) {e.printStackTrace();}
            }
        });
	}
	public void IniciarComponents(ControladorPartida controlP) {
		controladorPartida = controlP;
		controladorPartida.RefrescarPartida();
	}
	@Override
	public void actionPerformed(ActionEvent ae) {
		JButton button = (JButton) ae.getSource();
		EstatPartida estat = controladorPartida.GetEstat();
		if(estat==EstatPartida.EnCurs) {
			switch(button.getText()) {
			case textRendirse: controladorPartida.Rendirse();break;
			case textDemanarTaules: controladorPartida.ProposarTaules();break;
			case textPassarTorn: controladorPartida.CanviarTorn();break;
			case textBufar: controladorPartida.ProposarBufar();break;
			case textRefrescar: controladorPartida.RefrescarPartida();break;
			default:ClickCasella((JButton)ae.getSource());break;
			}
		}
		else if(estat==EstatPartida.Empat)JOptionPane.showMessageDialog(null, "Partida empatada.\nPrem sortir per tornar al menú.");
		else if((estat==EstatPartida.GuanyenBlanques)==controladorPartida.SocBlanc())JOptionPane.showMessageDialog(null, "Has guanyat la partida!\nPrem sortir per tornar al menú.");
		else JOptionPane.showMessageDialog(null, "Has perdut la partida.\nPrem sortir per tornar al menú.");
	}
	
	private void ClickCasella(JButton button) {
		if(controladorPartida.SeleccionarCasella(botons.get(button))) {
			activeButton = button;
			activeButton.setBorderPainted(true);
		}
		else if(activeButton!=null) activeButton.setBorderPainted(false);
		
	}
	
	private static void CanviarAReina(Coordenada which, boolean blanc) {
		GetButton(which).setIcon(blanc? ImgBlancaReina:ImgVermellaReina);
	}
	private static void CanviarAFitxa(Coordenada which, boolean blanc) {
		GetButton(which).setIcon(blanc? ImgBlanca:ImgVermella);
	}
	private static void CanviarABuit(Coordenada which) {
		GetButton(which).setIcon(null);
	}
	
	private static JButton GetButton(Coordenada which) {
		for(JButton b: botons.keySet()) if(botons.get(b).equals(which))return b;
		return null;
	}
	
	public static void PintarTaulell(char[][] taulell) {
		for(int y=0;y<8;++y)for(int x=0;x<8;++x) {
			Coordenada coor = new Coordenada(x,y);
			switch(taulell[y][x]) {
			case 'r': CanviarAFitxa(coor, false);break;
			case 'w': CanviarAFitxa(coor, true);break;
			case 'R': CanviarAReina(coor, false);break;
			case 'W': CanviarAReina(coor, true);break;
			default: CanviarABuit(coor);
			}
		}
	}
	public void CanviarTitol(String nouTitol) {frame.setTitle(nouTitol);}
	
	//ultim métode que s'executa al tancar la finestra
	private void TornarAlMenu() {
		new MenuInicial(controladorPartida.GetJugador());
		dispose();
		frame.dispose();
	}

}

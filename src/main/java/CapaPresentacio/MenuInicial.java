package CapaPresentacio;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import CapaDomini.Jugador;
import CapaDomini.Partida;
import CapaDomini.Solicitud;
import CapaDomini.Partida.EstatPartida;
import CapaPersistencia.BBDDJugador;
import CapaPersistencia.BBDDPartida;
import CapaPersistencia.BBDDSolicituds;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;

import CapaAplicacio.ControladorPartida;

public class MenuInicial extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;

	//icones per als botons
	private static final ImageIcon imgAcceptarSolicitud = new ImageIcon(new ImageIcon("AcceptarSolicitud.png").getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH));
	private static final ImageIcon imgDenegarSolicitud = new ImageIcon(new ImageIcon("DenegarSolicitud.png").getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH));
	private static final ImageIcon imgEnviarSolicitud = new ImageIcon(new ImageIcon("EnviarSolicitud.png").getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH));
	private static final ImageIcon imgVeureEstadistiques = new ImageIcon(new ImageIcon("VeureEstadistiques.png").getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH));
	private static final ImageIcon imgRefrescar = new ImageIcon(new ImageIcon("Refrescar.png").getImage().getScaledInstance(35, 35, Image.SCALE_SMOOTH));
	
	//contenidors estàtics de botons dinàmics
	private JPanel usersPanel, myTurnPanel, notMyTurnPanel, receivedPanel, sentPanel;
	//textos semipermanents
	private JLabel lblNoUsers,  lblNoMine,   lblNoWaiting,  lblNoReceived, lblNoSent;
	//missatges per diferenciar botons del mateix contenidor
	private final String txtAcceptarSolicitud = "acceptar sol·licitud";
	private final String txtDenegarSolicitud = "denegar sol·licitud";
	private final String txtEnviarSolicitud = "enviar sol·licitud de partida";
	private final String txtVeureEstadistiques = "veure estadístiques contra aquest jugador";
	
	//per diferenciar quan la finestra es tanca per sortir, o per jugar en una partida
	private boolean playing;
	
	//botons dinàmics de la interfície
	private HashMap<JButton, Jugador> botonsUsuaris;
	private HashMap<JButton, Solicitud> botonsSolicitudsEnviades, botonsSolicitudsRebudes;
	private HashMap<JButton, Partida> botonsPartidesTornMeu, botonsPartidesNoTornMeu;
	
	private MenuInicial frame;
	private Jugador usuari;
	
	public MenuInicial() {
		getContentPane().setBackground(new Color(85, 107, 47));
		getContentPane().setLayout(new GridLayout(1, 3, 0, 10));
		
		usersPanel = new JPanel();
		usersPanel.setBackground(new Color(107, 142, 35));
		getContentPane().add(usersPanel);
		usersPanel.setLayout(null);
		
		JLabel lblUsuaris = new JLabel("Usuaris Online");
		lblUsuaris.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuaris.setFont(new Font("Poor Richard", Font.PLAIN, 40));
		lblUsuaris.setBounds(75, 10, 250, 50);
		usersPanel.add(lblUsuaris);
		
		lblNoUsers = new JLabel("no hi ha cap usuari connectat");
		lblNoUsers.setForeground(Color.DARK_GRAY);
		lblNoUsers.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoUsers.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		lblNoUsers.setBounds(75, 75, 250, 50);
		usersPanel.add(lblNoUsers);
		
		JButton btnRefrescar = new JButton();
		btnRefrescar.setBounds(0, 720, 40, 40);
		btnRefrescar.setIcon(imgRefrescar);
		btnRefrescar.addActionListener(this);
		usersPanel.add(btnRefrescar);
		
		JPanel gamesPanelHolder = new JPanel();
		getContentPane().add(gamesPanelHolder);
		gamesPanelHolder.setLayout(new GridLayout(2, 1, 0, 0));
		
		myTurnPanel = new JPanel();
		myTurnPanel.setBackground(new Color(147, 112, 219));
		gamesPanelHolder.add(myTurnPanel);
		myTurnPanel.setLayout(null);
		
		JLabel lblTorn = new JLabel("Partides Pendents");
		lblTorn.setHorizontalAlignment(SwingConstants.CENTER);
		lblTorn.setFont(new Font("Poor Richard", Font.PLAIN, 40));
		lblTorn.setBounds(55, 10, 290, 50);
		myTurnPanel.add(lblTorn);
		
		lblNoMine = new JLabel("no hi ha cap partida on sigui el teu torn");
		lblNoMine.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoMine.setForeground(Color.DARK_GRAY);
		lblNoMine.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		lblNoMine.setBounds(55, 75, 290, 50);
		myTurnPanel.add(lblNoMine);
		
		notMyTurnPanel = new JPanel();
		notMyTurnPanel.setBackground(new Color(186, 85, 211));
		gamesPanelHolder.add(notMyTurnPanel);
		notMyTurnPanel.setLayout(null);
		
		JLabel lblNoTorn = new JLabel("Partides Esperant");
		lblNoTorn.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoTorn.setFont(new Font("Poor Richard", Font.PLAIN, 40));
		lblNoTorn.setBounds(55, 10, 290, 50);
		notMyTurnPanel.add(lblNoTorn);
		
		lblNoWaiting = new JLabel("no hi ha cap partida on no sigui el teu torn");
		lblNoWaiting.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoWaiting.setForeground(Color.DARK_GRAY);
		lblNoWaiting.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		lblNoWaiting.setBounds(45, 75, 310, 50);
		notMyTurnPanel.add(lblNoWaiting);
		
		JPanel requestsPanelHolder = new JPanel();
		getContentPane().add(requestsPanelHolder);
		requestsPanelHolder.setLayout(new GridLayout(2, 1, 0, 0));
		
		receivedPanel = new JPanel();
		receivedPanel.setBackground(new Color(70, 130, 180));
		requestsPanelHolder.add(receivedPanel);
		receivedPanel.setLayout(null);
		
		JLabel lblRebuts = new JLabel("Reptes Rebuts");
		lblRebuts.setHorizontalAlignment(SwingConstants.CENTER);
		lblRebuts.setFont(new Font("Poor Richard", Font.PLAIN, 40));
		lblRebuts.setBounds(75, 10, 250, 50);
		receivedPanel.add(lblRebuts);
		
		lblNoReceived = new JLabel("no has rebut cap sol·licitud nova");
		lblNoReceived.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoReceived.setForeground(Color.DARK_GRAY);
		lblNoReceived.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		lblNoReceived.setBounds(75, 75, 250, 50);
		receivedPanel.add(lblNoReceived);
		
		sentPanel = new JPanel();
		sentPanel.setBackground(new Color(95, 158, 160));
		requestsPanelHolder.add(sentPanel);
		sentPanel.setLayout(null);
		
		JLabel lblEnviats = new JLabel("Reptes Enviats");
		lblEnviats.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnviats.setFont(new Font("Poor Richard", Font.PLAIN, 40));
		lblEnviats.setBounds(75, 10, 250, 50);
		sentPanel.add(lblEnviats);
		
		lblNoSent = new JLabel("no tens cap sol·licitud pendent");
		lblNoSent.setHorizontalAlignment(SwingConstants.CENTER);
		lblNoSent.setForeground(Color.DARK_GRAY);
		lblNoSent.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		lblNoSent.setBounds(75, 75, 250, 50);
		sentPanel.add(lblNoSent);

		botonsSolicitudsEnviades = new HashMap<JButton, Solicitud>();
		botonsSolicitudsRebudes = new HashMap<JButton, Solicitud>();
		botonsPartidesTornMeu = new HashMap<JButton, Partida>();
		botonsPartidesNoTornMeu = new HashMap<JButton, Partida>();
		botonsUsuaris = new HashMap<JButton, Jugador>();
	}
	
	public MenuInicial(Jugador jugador) {
		usuari = jugador;
		EventQueue.invokeLater(new Runnable() {
        public void run() {
            try {
            	frame = new MenuInicial();
            	frame.setSize(1200, 800);
            	frame.setTitle("Benvingut a Dames!©, "+usuari.GetNom());
            	frame.setResizable(false);
            	frame.setLocationRelativeTo(null);
            	frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                frame.setVisible(true);
                frame.addWindowListener(new WindowAdapter()
                {
                    public void windowClosing(WindowEvent e)
                    {
                    	TancarSessió();
                    }
                });
                frame.IniciarComponents(usuari);
                } catch (Exception e) {e.printStackTrace();}
            }
        });
	}
	
	public void IniciarComponents(Jugador jugador) {
		usuari = jugador;
		playing = false;
		Refrescar();
	}

	
	private void Refrescar() {
		try {
			ReiniciarComponents();
			ArrayList<Partida> partides = usuari.RefrescarPartides();
			ArrayList<Solicitud> solicituds = usuari.RefrescarSolicituds();
			ArrayList<Jugador> jugadorsNoElegibles = new ArrayList<Jugador>();
			jugadorsNoElegibles.add(usuari);
			for(Solicitud solicitud: solicituds) {
				if(solicitud.SocEmisor(usuari)) {
					PosarASolicitudsEnviades(solicitud);
					jugadorsNoElegibles.add(solicitud.GetJugadorReceptor());
				}
				else {
					PosarASolicitudsRebudes(solicitud);
					jugadorsNoElegibles.add(solicitud.GetJugadorEmisor());
				}
			}
			for(Partida partida: partides) {
				if(partida.GetEstat()==EstatPartida.EnCurs) {
					if(partida.EsElMeuTorn(usuari)) {
						PosarAPartidesMeves(partida);
						jugadorsNoElegibles.add(partida.GetTornBlanques()? partida.GetVermelles():partida.GetBlanques());
					}
					else {
						PosarAPartidesNoMeves(partida);
						jugadorsNoElegibles.add(partida.GetTornBlanques()? partida.GetBlanques():partida.GetVermelles());
					}
				}
				//TODO else contar estadístiques
			}
			for(Jugador jugador:BBDDJugador.GetJugadorsOnline()) if(!jugadorsNoElegibles.contains(jugador)) PosarAJugadors(jugador);
			lblNoSent.setVisible(botonsSolicitudsEnviades.isEmpty());
			lblNoReceived.setVisible(botonsSolicitudsRebudes.isEmpty());
			lblNoMine.setVisible(botonsPartidesTornMeu.isEmpty());
			lblNoWaiting.setVisible(botonsPartidesNoTornMeu.isEmpty());
			lblNoUsers.setVisible(botonsUsuaris.isEmpty());
			repaint();
		}
		catch (Exception e) {e.printStackTrace();}
		
	}
	
	private void ReiniciarComponents() {
		int cUsersPanel = usersPanel.getComponentCount();
		int cMyTurnPanel = myTurnPanel.getComponentCount();
		int cNotMyTurnPanel = notMyTurnPanel.getComponentCount();
		int cReceivedPanel = receivedPanel.getComponentCount();
		int cSentPanel = sentPanel.getComponentCount();
		for(int i=3;i<cUsersPanel;++i)usersPanel.remove(3);
		for(int i=2;i<cMyTurnPanel;++i)myTurnPanel.remove(2);
		for(int i=2;i<cNotMyTurnPanel;++i)notMyTurnPanel.remove(2);
		for(int i=2;i<cReceivedPanel;++i)receivedPanel.remove(2);
		for(int i=2;i<cSentPanel;++i)sentPanel.remove(2);
		botonsSolicitudsEnviades = new HashMap<JButton, Solicitud>();
		botonsSolicitudsRebudes = new HashMap<JButton, Solicitud>();
		botonsPartidesTornMeu = new HashMap<JButton, Partida>();
		botonsPartidesNoTornMeu = new HashMap<JButton, Partida>();
		botonsUsuaris = new HashMap<JButton, Jugador>();
	}
	
	private void PosarAJugadors(Jugador jugador) {
		int distancia = 30*botonsUsuaris.keySet().size()/2;
		JLabel nomJugador = new JLabel(jugador.GetNom());
		nomJugador.setHorizontalAlignment(SwingConstants.CENTER);
		nomJugador.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		nomJugador.setBounds(100, 100+distancia, 100, 25);
		usersPanel.add(nomJugador);
		JButton btnSolicitar = new JButton();
		btnSolicitar.setIcon(imgEnviarSolicitud);
		btnSolicitar.setBounds(205, 95+distancia, 30, 30);
		btnSolicitar.setToolTipText(txtEnviarSolicitud);
		btnSolicitar.addActionListener(this);
		usersPanel.add(btnSolicitar);
		botonsUsuaris.put(btnSolicitar, jugador);
		JButton btnStats = new JButton();
		btnStats.setIcon(imgVeureEstadistiques);
		btnStats.setBounds(240, 95+distancia, 30, 30);
		btnStats.setToolTipText(txtVeureEstadistiques);
		btnStats.addActionListener(this);
		usersPanel.add(btnStats);
		botonsUsuaris.put(btnStats, jugador);
	}
	private void PosarAPartidesMeves(Partida partida) {
		int distancia = 30*botonsPartidesTornMeu.keySet().size();
		JButton btnPartida = new JButton("VS "+(partida.GetTornBlanques()? partida.GetJugadorVermelles():partida.GetJugadorBlanques()));
		btnPartida.setBounds(50, 100+distancia, 300, 30);
		btnPartida.setBackground(new Color(85,185,125));
		btnPartida.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		btnPartida.setToolTipText("entrar en partida");
		btnPartida.addActionListener(this);
		myTurnPanel.add(btnPartida);
		botonsPartidesTornMeu.put(btnPartida, partida);
	}
	private void PosarAPartidesNoMeves(Partida partida) {
		int distancia = 30*botonsPartidesNoTornMeu.keySet().size();
		JButton btnPartida = new JButton("VS "+(partida.GetTornBlanques()? partida.GetJugadorBlanques():partida.GetJugadorVermelles()));
		btnPartida.setBounds(50, 100+distancia, 300, 30);
		btnPartida.setBackground(new Color(85,125,185));
		btnPartida.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		btnPartida.setToolTipText("veure la partida");
		btnPartida.addActionListener(this);
		notMyTurnPanel.add(btnPartida);
		botonsPartidesNoTornMeu.put(btnPartida, partida);
	}
	private void PosarASolicitudsEnviades(Solicitud solicitud) {
		int distancia = 35*botonsSolicitudsEnviades.keySet().size();
		JButton btnSolicitud = new JButton("Sol·licitud enviada a "+solicitud.GetReceptor());
		btnSolicitud.setBounds(50, 100+distancia, 300, 30);
		btnSolicitud.setBackground(new Color(125,125,185));
		btnSolicitud.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		btnSolicitud.setToolTipText("cancelar sol·licitud");
		btnSolicitud.addActionListener(this);
		sentPanel.add(btnSolicitud);
		botonsSolicitudsEnviades.put(btnSolicitud, solicitud);
	}
	private void PosarASolicitudsRebudes(Solicitud solicitud) {
		int distancia = 30*botonsSolicitudsRebudes.keySet().size()/2;
		JLabel nomJugador = new JLabel(solicitud.GetEmisor());
		nomJugador.setHorizontalAlignment(SwingConstants.CENTER);
		nomJugador.setFont(new Font("Poor Richard", Font.PLAIN, 20));
		nomJugador.setBounds(100, 100+distancia, 100, 25);
		receivedPanel.add(nomJugador);
		JButton btnAcceptar = new JButton();
		btnAcceptar.setIcon(imgAcceptarSolicitud);
		btnAcceptar.setBounds(205, 95+distancia, 30, 30);
		btnAcceptar.setToolTipText(txtAcceptarSolicitud);
		btnAcceptar.addActionListener(this);
		receivedPanel.add(btnAcceptar);
		botonsSolicitudsRebudes.put(btnAcceptar, solicitud);
		JButton btnDenegar = new JButton();
		btnDenegar.setIcon(imgDenegarSolicitud);
		btnDenegar.setBounds(240, 95+distancia, 30, 30);
		btnDenegar.setToolTipText(txtDenegarSolicitud);
		btnDenegar.addActionListener(this);
		receivedPanel.add(btnDenegar);
		botonsSolicitudsRebudes.put(btnDenegar, solicitud);
	}
	
	private void TancarSessió() {
		if(!playing) {
			usuari.CanviarSessio(false);
			MenuRegistre.main(null);
		}
		frame.dispose();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			JButton button = (JButton) e.getSource();
			if(botonsUsuaris.containsKey(button)) {
				if(button.getToolTipText().equals(txtEnviarSolicitud))
						EnviarSolicitud(botonsUsuaris.get(button));
				else VeureEstadistiques(botonsUsuaris.get(button));
			}
			else if(botonsSolicitudsEnviades.containsKey(button)) CancelarSolicitud(botonsSolicitudsEnviades.get(button));
			else if(botonsSolicitudsRebudes.containsKey(button)) {
				if(button.getToolTipText().equals(txtAcceptarSolicitud)) AcceptarSolicitud(botonsSolicitudsRebudes.get(button));
				else DenegarSolicitud(botonsSolicitudsRebudes.get(button));
			}
			else if(botonsPartidesTornMeu.containsKey(button)) EntrarEnPartida(botonsPartidesTornMeu.get(button));
			else if(botonsPartidesNoTornMeu.containsKey(button)) EntrarEnPartida(botonsPartidesNoTornMeu.get(button));
			Refrescar();
		}
		catch (Exception e1) {e1.printStackTrace();}
	}
	//funcions cridades pels botons, menys refrescar (que es crida s'apreti el botó que s'apreti)
	private void EnviarSolicitud(Jugador jugador) throws Exception{
		Solicitud solicitud = new Solicitud(usuari, jugador);
		if(!BBDDSolicituds.ExisteixSolicitud(solicitud)) {
			BBDDSolicituds.RegistrarNovaSolicitud(solicitud);
			JOptionPane.showMessageDialog(null, "S'ha enviat una sol·licutd de partida a "+jugador.GetNom()+".\nAra només cal esperar");
		}
		else JOptionPane.showMessageDialog(null, "Ja existeix una sol·licitud pendent entre "+jugador.GetNom()+" i vosté!");
	}
	private void VeureEstadistiques(Jugador jugador) {
		int[] stats = usuari.GetEstadistiquesContra(jugador);
		String missatge;
		if(stats[0]!=0) {
			missatge =     "Partides jugades: "+stats[0]
						+"\nPartides Guanyades: "+stats[1]
						+"\nPartides Empatades: "+(stats[0]-stats[1]-stats[2])
						+"\nPartides Perdudes: "+stats[2]
						+"\nPercentatge de Victória: "+(stats[1]==0&&stats[2]==0? "-":(100f*stats[1])/(stats[1]+stats[2]))+"%";
		}
		else missatge = "Encara no has acabat cap partida amb aquest jugador.";
		JOptionPane.showMessageDialog(null, missatge);
	}
	private void CancelarSolicitud(Solicitud solicitud) throws Exception{
		if(BBDDSolicituds.EliminarSolicitud(solicitud)) JOptionPane.showMessageDialog(null, "S'ha cancel·lat la sol·licitud de partida amb "+solicitud.GetEmisor());
		else JOptionPane.showMessageDialog(null, "Aquesta sol·licitud ha caducat");
	}
	private void AcceptarSolicitud(Solicitud solicitud) throws Exception{
		if(!BBDDPartida.ExisteixPartidaEntre(solicitud.GetJugadorEmisor(), solicitud.GetJugadorReceptor())) {
			if(BBDDSolicituds.ExisteixSolicitud(solicitud)) {
				Partida partida = new Partida(solicitud.GetJugadorReceptor(), solicitud.GetJugadorEmisor());
				BBDDSolicituds.EliminarSolicitud(solicitud);
				JOptionPane.showMessageDialog(null, "Sol·licitud acceptada correctament!\nComençant partida amb "+solicitud.GetEmisor()+". Bona sort, vas primer!");
				EntrarEnPartida(partida);
			}
			else JOptionPane.showMessageDialog(null, "Aquesta sol·licitud ha caducat");
		}
		else JOptionPane.showMessageDialog(null, "Ha passat quelcom molt extrany, ja sembla existir una partida entre vosté i "+solicitud.GetEmisor()+"...");
	}
	private void DenegarSolicitud(Solicitud solicitud) throws Exception{
		if(BBDDSolicituds.ExisteixSolicitud(solicitud)) {
			BBDDSolicituds.EliminarSolicitud(solicitud);
			JOptionPane.showMessageDialog(null, "S'ha denegat la sol·licitud de partida de "+solicitud.GetEmisor());
		}
		else JOptionPane.showMessageDialog(null, "Aquesta sol·licitud ha caducat");
	}
	private void EntrarEnPartida(Partida partida) {
		playing = true;
		new ControladorPartida(partida, usuari);
		dispose();
	}
}

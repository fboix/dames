package CapaPersistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import CapaDomini.Jugador;

public final class BBDDJugador {

	//Métodes d'inserció de dades
    public static void InsertJugador(Jugador jugador) throws Exception {
		Connection con = ConnexioBBDD.connexio();
    	try {
            String sql = "Insert into Jugador (nom, pass, conectat) values (?, ?, ?)";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, jugador.GetNom());
            pst.setString(2, jugador.GetContrassenya());
            pst.setString(3, jugador.IsOnline()? "t":"f" );
            if (pst.executeUpdate() != 1) throw new Exception("La inserció del jugador no s'ha fet a la BBDD");
            pst.close();
    	}
        catch (SQLException e) {throw new Exception("Error al inserir el jugador "+jugador.GetNom()+": " + e.getMessage());}
        finally {
        	con.close();
        }
    }
    
    //Métodes de modificació de dades
    public static void ActualitzarSessio(Jugador jugador) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	String sql = "UPDATE Jugador SET conectat = ? WHERE nom = ?";
        	PreparedStatement pst = con.prepareStatement(sql);
        	pst.setString(1, jugador.IsOnline()? "t":"f");
        	pst.setString(2, jugador.GetNom());
            if (pst.executeUpdate() != 1) throw new Exception("La actualització de la sessió no s'ha fet a la BBDD");
            pst.close();
            
        } catch (SQLException e) {throw new Exception("Error al intentar actualitzar la sessió del jugador "+jugador.GetNom()+": " + e);}
        finally {
        	con.close();
        }
    }
    
    //Métodes d'obtenció de dades
    public static boolean ExisteixJugador(String nom) throws Exception {
		Connection con = ConnexioBBDD.connexio();
        PreparedStatement pst;
        ResultSet rs;
        try {
            pst = con.prepareStatement("SELECT * FROM Jugador WHERE nom = ?");
            pst.setString(1, nom);
            rs = pst.executeQuery();
            if (rs.next()) {
                pst.close();
                rs.close();
                return true;
            }
            else {
                pst.close();
                rs.close();
            	return false;
            }
        } 
        catch (SQLException e) {throw new Exception("Error al buscar el jugador "+nom+": " + e.getMessage());}
        finally {
        	con.close();
        }
    }
    public static Jugador GetJugador(String nom, boolean omplirTot) throws Exception {
		Connection con = ConnexioBBDD.connexio();
        PreparedStatement pst;
        ResultSet rs;
        try {
            pst = con.prepareStatement("SELECT * FROM Jugador WHERE nom = ?");
            pst.clearParameters();
            pst.setString(1, nom);
            rs = pst.executeQuery();
            if (rs.next()) {
            	Jugador jugador = new Jugador(nom, rs.getString(2), rs.getString(3).equals("t"));
            	if(omplirTot) {
                	BBDDPartida.OmplirPartidesJugador(jugador);
                	BBDDSolicituds.OmplirSolicitudsJugador(jugador);
            	}
            	pst.close();
            	rs.close();
            	return jugador;
            }
            else {
            	pst.close();
            	rs.close();
            	return null;
            }
        } 
        catch (SQLException e) {throw new Exception("Error al obtenir el jugador "+nom+": " + e.getMessage());}
        finally {
        	con.close();
        }
    }
    public static ArrayList<Jugador> GetJugadorsOnline() throws Exception{
		Connection con = ConnexioBBDD.connexio();
        PreparedStatement pst;
        ResultSet rs;
        try {
        	ArrayList<Jugador> result = new ArrayList<Jugador>();
            pst = con.prepareStatement("SELECT * FROM Jugador WHERE conectat = ?");
            pst.setString(1, "t");
            rs = pst.executeQuery();
            while (rs.next()) {
            	Jugador jugador = new Jugador(rs.getString(1), rs.getString(2), rs.getString(3).equals("t"));
            	BBDDPartida.OmplirPartidesJugador(jugador);
            	BBDDSolicituds.OmplirSolicitudsJugador(jugador);
            	result.add(jugador);
            }
            pst.close();
            rs.close();
            return result;
        } 
        catch (SQLException e) {throw new Exception("Error al intentar obtenir jugadors connectats: " + e.getMessage());}
        finally {
        	con.close();
        }
    }
	
}

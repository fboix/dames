package CapaPersistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import CapaDomini.Coordenada;
import CapaDomini.Taulell;

public class BBDDTaulell {
	
	//Métodes de creació i destrucció de dades
	public static int RegistrarNouTaulell(Taulell taulell) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pstm = con.prepareStatement("SELECT * FROM Taulell ORDER BY id_taulell ASC");
        	ResultSet rs = pstm.executeQuery();
            int foundSpot = 0; //trobar el primer forat lliure en una llista d'ID d'ordre ascendent per assignar la nova
            int previousID = 0;
            boolean found = false;
            while (rs.next()&&!found) {
            	int currentID = rs.getInt(1);
            	if(currentID-previousID > 1) {
            		foundSpot = previousID+1;
            		found = true;
            	}
            	previousID = currentID;
            }
            if(!found)foundSpot = previousID+1;
            pstm.close();
            rs.close();
            InsertTaulell(taulell, foundSpot);
            return foundSpot;
            
        } catch (SQLException e) {throw new Exception("Error al intentar registrar el nou taulell: " + e);}
        finally {
        	con.close();
        }
	}
	private static void InsertTaulell(Taulell taulell, int id_taulell) throws Exception {
		Connection con = ConnexioBBDD.connexio();
	    try {
	        String sql = "Insert into Taulell (id_taulell, historial, caselles, lastMove, blanquesRestants, vermellesRestants) values (?, ?, ?, ?, ?, ?)";
	        PreparedStatement pst = con.prepareStatement(sql);
	        pst.setInt(1, id_taulell);
	        pst.setInt(2, taulell.GetHistorial());
	        pst.setString(3, taulell.toString());
	        pst.setString(4, taulell.GetLastMove());
	        pst.setInt(5, taulell.GetBlanquesRestants());
	        pst.setInt(6, taulell.GetVermellesRestants());
	        if (pst.executeUpdate() != 1) throw new Exception("La inserció del taulell no s'ha fet a la BBDD");
	        pst.close();
	    } catch (SQLException e) {throw new Exception("Error al intentar inserir el taulell: " + e);}
        finally {
        	con.close();
        }
	}
	public static void EliminarTaulell(Taulell taulell) throws Exception {
		Connection con = ConnexioBBDD.connexio();
        try {
            String sql = "DELETE FROM Taulell WHERE id_taulell = ?";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, taulell.GetIDTaulell());
            if (pst.executeUpdate() != 1) throw new Exception("La eliminació del taulell no s'ha realitzat a la BBDD");
            pst.close();
            
        } catch (SQLException e) {throw new Exception("Error al intentar eliminar el taulell: " + e);}
        finally {
        	con.close();
        }
	}
	
	//Mëtodes de modificació de dades
	public static void ActualitzarTaulell(Taulell taulell) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	String sql = "UPDATE Taulell SET caselles = ?, lastMove = ?, blanquesRestants = ?, vermellesRestants = ? WHERE id_taulell = ?";
        	PreparedStatement pst = con.prepareStatement(sql);
        	pst.setString(1, taulell.toString());
        	pst.setString(2, taulell.GetLastMove());
        	pst.setInt(3, taulell.GetBlanquesRestants());
        	pst.setInt(4, taulell.GetVermellesRestants());
        	pst.setInt(5, taulell.GetIDTaulell());
            if (pst.executeUpdate() != 1) throw new Exception("La actualització del taulell no s'ha guardat a la BBDD");
            pst.close();
            
        } catch (SQLException e) {throw new Exception("Error al intentar actualitzar el taulell" + e);}
        finally {
        	con.close();
        }
	}
	
	//Métodes d'obtenció de dades
	public static Taulell GetTaulell(int IDTaulell) throws Exception {
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pst = con.prepareStatement("SELECT * FROM Taulell WHERE id_taulell = ?");
            pst.clearParameters();
            pst.setInt(1, IDTaulell);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
            	String[] coorString = rs.getString(4).split(",");
            	Coordenada coordenada = new Coordenada(Integer.parseInt(coorString[0]), Integer.parseInt(coorString[1]));
            	Taulell taulell = new Taulell(IDTaulell,StringToCaselles(rs.getString(3)), coordenada, BBDDHistorialTaulell.GetHistorial(rs.getInt(2)), rs.getInt(5),rs.getInt(6));
            	pst.close();
            	rs.close();
            	return taulell;
            }
            else {
            	pst.close();
            	rs.close();
            	return null;
            }
        } catch (SQLException e) { throw new Exception("Error al intentar trobar el taulell: " + e);}
        finally {
        	con.close();
        }
	}
	
	public static char[][] StringToCaselles(String seq){
		char[][] caselles = new char[8][8];
		String[] rows = seq.split(";");
		for(int y=0;y<rows.length;++y) {
			String[] squares = rows[y].split(",");
			for(int x=0;x<squares.length;++x) caselles[y][x]=squares[x].charAt(0);
		}
		return caselles;
	}
}

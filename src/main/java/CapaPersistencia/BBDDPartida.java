package CapaPersistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import CapaDomini.Jugador;
import CapaDomini.Partida;
import CapaDomini.Partida.EstatPartida;
import CapaDomini.Taulell;

public final class BBDDPartida {
	
	//Métodes de creació de dades
	public static int RegistrarNovaPartida(Partida partida) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pstm = con.prepareStatement("SELECT * FROM Partida ORDER BY id_partida ASC");
        	ResultSet rs = pstm.executeQuery();
            int foundSpot = 0; //trobar el primer forat lliure en una llista d'ID d'ordre ascendent per assignar la nova
            int previousID = 0;
            boolean found = false;
            while (rs.next()&&!found) {
            	int currentID = rs.getInt(1);
            	if(currentID-previousID > 1) {
            		foundSpot = previousID+1;
            		found = true;
            	}
            	previousID = currentID;
            }
            if(!found)foundSpot = previousID+1;
            pstm.close();
            rs.close();
            InsertPartida(partida, foundSpot);
            return foundSpot;
            
        } catch (SQLException e) {throw new Exception("Error al intentar registrar la nova partida: " + e);}
        finally {
        	con.close();
        }
	}
	private static void InsertPartida(Partida partida, int id_partida) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
            String sql = "Insert into Partida (id_partida, taulell, jugadorBlanques, jugadorVermelles, estat, movimentsFets, movimentsAnteriors, partidaComencada, taulesProposades, tornBlanques, bufat)"
            			+"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setInt(1, id_partida);
            pst.setInt(2, partida.GetTaulell());
            pst.setString(3, partida.GetJugadorBlanques());
            pst.setString(4, partida.GetJugadorVermelles());
            pst.setString(5, partida.GetEstatPartida());
            pst.setString(6, partida.GetMovimentsFets());
            pst.setString(7, partida.GetMovimentsAnteriors());
            pst.setString(8, partida.GetPartidaComencada()? "t":"f");
            pst.setString(9, partida.GetTaulesProposades()? "t":"f");
            pst.setString(10, partida.GetTornBlanques()? "t":"f");
            pst.setString(11, partida.GetBufat()? "t":"f");
            if (pst.executeUpdate() != 1) throw new Exception("La inserció de la partida no s'ha fet a la BBDD");
            pst.close();
        } catch (SQLException e) {throw new Exception("Error al intentar registrar la nova partida: " + e);}
        finally {
        	con.close();
        }
	}
	
	//Métodes de modificació de dades
	public static void OmplirPartidesJugador(Jugador jugador) throws Exception {
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pst = con.prepareStatement("SELECT * FROM Partida WHERE jugadorBlanques = ? OR jugadorVermelles = ? ORDER BY id_partida ASC");
            pst.clearParameters();
            pst.setString(1, jugador.GetNom());
            pst.setString(2, jugador.GetNom());
            ResultSet rs = pst.executeQuery();
            while (rs.next()) jugador.AfegirPartida(FerPartidaDesdeResultat(rs));
            pst.close();
            rs.close();
        } catch (SQLException e) {throw new Exception("Error al intentar omplir les partides del jugador "+jugador.GetNom()+": " + e);}
        finally {
        	con.close();
        }
	}
	
	public static void ActualitzarPartida(Partida partida) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	String sql = "UPDATE Partida SET estat = ?, movimentsFets = ?, movimentsAnteriors = ?, partidaComencada = ?, taulesProposades = ?, tornBlanques = ?, bufat = ? WHERE id_partida = ?";
        	PreparedStatement pst = con.prepareStatement(sql);
        	pst.setString(1, partida.GetEstatPartida());
        	pst.setString(2, partida.GetMovimentsFets());
        	pst.setString(3, partida.GetMovimentsAnteriors());
        	pst.setString(4, partida.GetPartidaComencada()? "t":"f");
        	pst.setString(5, partida.GetTaulesProposades()? "t":"f");
        	pst.setString(6, partida.GetTornBlanques()? "t":"f");
        	pst.setString(7, partida.GetBufat()? "t":"f");
        	pst.setInt(8, partida.GetIdPartida());
            if (pst.executeUpdate() != 1) throw new Exception("La actualització de la partida no s'ha guardat a la BBDD");
            pst.close();
            
        } catch (SQLException e) {throw new Exception("Error al intentar actualitzar la partida" + e);}
        finally {
        	con.close();
        }
	}
	
	//Métodes d'obtenció de dades
	public static Partida GetPartida(int id_partida) throws Exception {
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pst = con.prepareStatement("SELECT * FROM Partida WHERE id_partida = ? ORDER BY id_partida ASC");
            pst.setInt(1, id_partida);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
            	Partida partida = FerPartidaDesdeResultat(rs);
            	pst.close();
            	rs.close();
            	return partida;
            }
        	pst.close();
        	rs.close();
        	return null;
        } catch (SQLException e) {throw new Exception("Error al intentar llegir la partida: " + e);}
        finally {
        	con.close();
        }
	}
	public static boolean ExisteixPartidaEntre(Jugador a, Jugador b) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pst = con.prepareStatement("SELECT * FROM Partida WHERE (jugadorBlanques = ? AND jugadorVermelles = ?) OR (jugadorBlanques = ? AND jugadorVermelles = ?)");
            pst.setString(1, a.GetNom());
            pst.setString(2, b.GetNom());
            pst.setString(3, b.GetNom());
            pst.setString(4, a.GetNom());
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
            	boolean existeix = Partida.GetEstat(rs.getString(5)).equals(EstatPartida.EnCurs);
            	pst.close();
            	rs.close();
            	return existeix;
            }
        	pst.close();
        	rs.close();
        	return false;
        } catch (SQLException e) {throw new Exception("Error al intentar llegir la partida: " + e);}
        finally {
        	con.close();
        }
	}
	
	private static Partida FerPartidaDesdeResultat(ResultSet rs) throws Exception {
		int id_partida = rs.getInt(1);
		Taulell taulell = BBDDTaulell.GetTaulell(rs.getInt(2));
		Jugador jugadorBlanques = BBDDJugador.GetJugador(rs.getString(3), false);
		Jugador jugadorVermelles = BBDDJugador.GetJugador(rs.getString(4), false);
		Partida.EstatPartida estat = Partida.GetEstat(rs.getString(5));
		String[] splitMovimentsFets = rs.getString(6).split(",");
		String[] splitMovimentsAnteriors = rs.getString(7).split(",");
		boolean[] movimentsFets = new boolean[]{splitMovimentsFets[0].equals("t"),splitMovimentsFets[1].equals("t")};
		boolean[] movimentsAnteriors = new boolean[]{splitMovimentsAnteriors[0].equals("t"),splitMovimentsAnteriors[1].equals("t")};
		boolean partidaComencada = rs.getString(8).equals("t");
		boolean taulesProposades = rs.getString(9).equals("t");
		boolean tornBlanques = rs.getString(10).equals("t");
		boolean bufat = rs.getString(11).equals("t");
		return new Partida(id_partida, taulell, jugadorBlanques, jugadorVermelles, estat, movimentsFets, movimentsAnteriors, partidaComencada, taulesProposades, tornBlanques, bufat);
	}
	
}

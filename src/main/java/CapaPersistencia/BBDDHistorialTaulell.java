package CapaPersistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import CapaDomini.Coordenada;
import CapaDomini.HistorialTaulell;

public final class BBDDHistorialTaulell {

	//Métodes de creació de dades
	public static int RegistrarNouHistorial(HistorialTaulell historial) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pstm = con.prepareStatement("SELECT * FROM Historial_Taulell ORDER BY id_historial ASC");
        	ResultSet rs = pstm.executeQuery();
            int foundSpot = 0; //trobar el primer forat lliure en una llista d'ID d'ordre ascendent per assignar la nova
            int previousID = 0;
            boolean found = false;
            while (rs.next()&&!found) {
            	int currentID = rs.getInt(1);
            	if(currentID-previousID > 1) {
            		foundSpot = previousID+1;
            		found = true;
            	}
            	previousID = currentID;
            }
            if(!found)foundSpot = previousID+1;
            pstm.close();
            rs.close();
            InsertHistorial(historial, foundSpot);
            return foundSpot;
            
        } catch (SQLException e) {throw new Exception("Error al intentar registrar el nou historial: " + e);}
        finally {
        	con.close();
        }
	}
	private static void InsertHistorial(HistorialTaulell historial, int id_historial) throws Exception{
		Connection con = ConnexioBBDD.connexio();
	    try {
	        String sql = "Insert into Historial_Taulell (id_historial, taulells) values (?, ?)";
	        PreparedStatement pst = con.prepareStatement(sql);
	        pst.setInt(1, id_historial);
	        pst.setString(2, historial.toString());
	        if (pst.executeUpdate() != 1) throw new Exception("La inserció de l'historial no s'ha fet a la BBDD");
	        pst.close();
	    } catch (SQLException e) {throw new Exception("Error al intentar registrar l'historial: " + e);}
        finally {
        	con.close();
        }
	}
	
	//Métodes de modificació de dades
	public static void ActualitzarHistorial(HistorialTaulell historial) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	String sql = "UPDATE Historial_Taulell SET taulells = ? WHERE id_historial = ?";
        	PreparedStatement pst = con.prepareStatement(sql);
        	pst.setString(1, historial.toString());
        	pst.setInt(2, historial.GetIDHistorial());
            if (pst.executeUpdate() != 1) throw new Exception("La actualització de l'historial no s'ha guardat a la BBDD");
            pst.close();
            
        } catch (SQLException e) {throw new Exception("Error al intentar actualitzar l'historial" + e);}
        finally {
        	con.close();
        }
	}
	
	//Métodes d'obtenció de dades
	public static HistorialTaulell GetHistorial(int id_historial) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pst = con.prepareStatement("SELECT * FROM Historial_Taulell WHERE id_historial = ?");
            pst.setInt(1, id_historial);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
            	HistorialTaulell historial = new HistorialTaulell(id_historial, StringToTaulells(rs.getString(2)));
            	pst.close();
            	rs.close();
            	return historial;
            }
        	pst.close();
        	rs.close();
        	return null;
        } catch (SQLException e) { throw new Exception("Error al intentar trobar l'historial: " + e);}
        finally {
        	con.close();
        }
	}
	
	private static ArrayList<Coordenada[]> StringToTaulells(String seq){
		ArrayList<Coordenada[]> resultat = new ArrayList<Coordenada[]>();
		if(seq!=null&&!seq.equals("")) {
			String[] moviments = seq.split(":");
			for(int i=0;i<moviments.length;++i) {
				String moviment = moviments[i];
				String[] coordenades = moviment.split(">");
				String[] strOrigen = coordenades[0].split(",");
				Coordenada origen = new Coordenada(Integer.parseInt(strOrigen[0]),Integer.parseInt(strOrigen[1]));
				Coordenada desti = null;
				if(coordenades[1].contains(",")) {
					String[] strDesti = coordenades[1].split(",");
					desti = new Coordenada(Integer.parseInt(strDesti[0]),Integer.parseInt(strDesti[1]));
				}
				resultat.add(new Coordenada[] {origen, desti});
			}
		}
		return resultat;
	}
	
	
}

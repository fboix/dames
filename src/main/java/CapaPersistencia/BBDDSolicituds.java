package CapaPersistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import CapaDomini.Jugador;
import CapaDomini.Solicitud;

public class BBDDSolicituds {
	
	//Métodes de creació i destrucció de dades
	public static void RegistrarNovaSolicitud(Solicitud solicitud) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
            String sql = "Insert into Solicitud (emisor, receptor) values (?, ?)";
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, solicitud.GetEmisor());
            pst.setString(2, solicitud.GetReceptor());
            if (pst.executeUpdate() != 1) throw new Exception("La inserció de la solicitud no s'ha fet a la BBDD");
            pst.close();
            
        } catch (SQLException e) {throw new Exception("Error al intentar registrar la nova solicitud: " + e);}
        finally {
        	con.close();
        }
	}
	public static boolean EliminarSolicitud(Solicitud solicitud) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
    		if(BBDDSolicituds.ExisteixSolicitud(solicitud)) {
                String sql = "Delete from Solicitud WHERE emisor = ? AND receptor = ?";
                PreparedStatement pst = con.prepareStatement(sql);
                pst.setString(1, solicitud.GetEmisor());
                pst.setString(2, solicitud.GetReceptor());
                if (pst.executeUpdate() != 1) throw new Exception("La eliminació de la solicitud no s'ha realitzat a la BBDD");
                pst.close();
                return true;
    		}
    		else return false;
            
        } catch (SQLException e) {throw new Exception("Error al intentar eliminar la solicitud: " + e);}
        finally {
        	con.close();
        }
	}
	
	//Métodes de modificació de dades
	public static void OmplirSolicitudsJugador(Jugador jugador) throws Exception {
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pst = con.prepareStatement("SELECT * FROM Solicitud WHERE emisor = ? OR receptor = ?");
            pst.setString(1, jugador.GetNom());
            pst.setString(2, jugador.GetNom());
            ResultSet rs = pst.executeQuery();
            while (rs.next()) {
            	if(rs.getString(1).equals(jugador.GetNom())) jugador.AfegirSolicitud(new Solicitud(jugador,BBDDJugador.GetJugador(rs.getString(2), false)));
            	else jugador.AfegirSolicitud(new Solicitud(BBDDJugador.GetJugador(rs.getString(1), false), jugador));
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {throw new Exception("Error al intentar omplir les solicituds del jugador "+jugador.GetNom()+": " + e);}
        finally {
        	con.close();
        }
	}
	
	//Métodes d'obtenció de dades
	public static boolean ExisteixSolicitud (Solicitud solicitud) throws Exception{
		Connection con = ConnexioBBDD.connexio();
        try {
        	PreparedStatement pst = con.prepareStatement("SELECT * FROM Solicitud WHERE emisor = ? AND receptor = ?");
            pst.setString(1, solicitud.GetEmisor());
            pst.setString(2, solicitud.GetReceptor());
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
            	pst.close();
            	rs.close();
            	return true;
            }
            pst.close();
            rs.close();
            return false;
        } catch (SQLException e) {throw new Exception("Error al intentar trobar la solicitud: " + e);}
        finally {
        	con.close();
        }
	}
}
